#!/bin/bash

# The spigot version
SPIGOT_VERSION="1.14.4"
# Whether to backup inbetween restarts
SPIGOT_BACKUP=true
# Backup Method [ "LOCAL", "NFS", "GIT" ]
SPIGOT_BACKUP_METHOD="NFS"
# Whether to backup server configs to git
SPIGOT_GIT_CONFIG_SYNC=true
# Whether to update inbetween restarts (builds new jar with buildtools)
SPIGOT_UPDATE=true
# Screen name
SPIGOT_SCREENNAME="spigot"
