#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "Usage $0 <command>"
	exit 1
fi

source ~/config/setup.sh

screen -S $SPIGOT_SCREENNAME -p 0 -X stuff "$1^M"
