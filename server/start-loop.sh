#!/bin/bash

while [ 1 ]
do
	cd ~/server
	./start.sh
	echo "Server stopped - waiting 10 seconds before restarting"
	sleep 10s
done
