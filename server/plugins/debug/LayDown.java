import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityToggleGlideEvent;

public class LayDown implements Debugger, Listener, Debugger.Stoppable {

	private Player p;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			return;
		}
		p = (Player) cs;
		p.setGliding(true);
	}

	@EventHandler
	public void changePose(EntityToggleGlideEvent event) {
		if (event.getEntity().getUniqueId().equals(p.getUniqueId())) {
			event.setCancelled(true);
		}
	}

	@Override
	public void stop() {
		
	}
}
