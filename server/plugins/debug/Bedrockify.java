import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.minecraft.server.v1_14_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.block.CraftBlock;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.*;

public class Bedrockify implements Debugger, Runnable {

	private RuntimeDebugger plugin;
	private Player p;
	private Queue<Location> locationQueue2;
	private Queue<Location> locationQueue;
	private int countdown = 600;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			cs.sendMessage("Run as player");
			return;
		}
		this.p = (Player) cs;
		this.plugin = plugin;

		findLocationsCloseToPlayer(this.p, 10);
	}

	private void findLocationsCloseToPlayer(Player player, int range) {
		Location playerLocation = player.getLocation().clone();

		Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
			Set<Location> locations = new TreeSet<>(Comparator.comparingDouble(playerLocation::distance));
			for (int x = -range; x <= range; x++) {
				for (int y = -range; y <= range; y++) {
					for (int z = -range; z <= range; z++) {
						Location loc = new Location(
								playerLocation.getWorld(),
								playerLocation.getBlockX() + x,
								playerLocation.getBlockY() + y,
								playerLocation.getBlockZ() + z
						);
						locations.add(loc);
					}
				}
			}
			this.locationQueue = new PriorityQueue<>(locations);
			TreeSet<Location> otherwayAround = new TreeSet<>(((TreeSet)locations).comparator().reversed());
			otherwayAround.addAll(locations);
			this.locationQueue2 = new PriorityQueue<>(otherwayAround);
			Bukkit.getScheduler().runTaskTimer(this.plugin, Bedrockify.this, 1L, 1L);
		});
	}

	@Override
	public void run() {
		if (countdown >= 0) {
			if (countdown % 20 == 0) {
				//this.p.sendTitle("Starting in " + (countdown / 20), null, 0, 20, 20);
			}
			this.countdown--;
			return;
		}
		if (!locationQueue.isEmpty()) {
			Location poll = null;
			while (poll == null || !poll.getBlock().getType().isSolid()) {
				poll = this.locationQueue.poll();
			}
			//this.p.sendTitle(null, "todo: " + this.locationQueue.size(), 0, 100, 20);
			sendBedrock(poll);
		} else if (!locationQueue2.isEmpty()) {
			Location poll = null;
			while (poll == null || !poll.getBlock().getType().isSolid()) {
				poll = this.locationQueue.poll();
			}
			sendFixed(poll);
		}
	}

	private void sendFixed(Location loc) {
		BlockPosition blockPosition = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		PacketPlayOutBlockChange blockChange = new PacketPlayOutBlockChange(new FakeBlockAccess(((CraftBlock)loc.getBlock()).getNMS().getBlock()), blockPosition);
		((CraftPlayer)this.p).getHandle().playerConnection.sendPacket(blockChange);
	}

	public void sendBedrock(Location loc) {
		BlockPosition blockPosition = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		PacketPlayOutBlockChange blockChange = new PacketPlayOutBlockChange(bedrockBlockAccess, blockPosition);
		((CraftPlayer)this.p).getHandle().playerConnection.sendPacket(blockChange);
	}

	private FakeBlockAccess bedrockBlockAccess = new FakeBlockAccess();

	static class FakeBlockAccess implements IBlockAccess {

		private Block block;

		public FakeBlockAccess() {
			this.block = Blocks.BEDROCK;
		}

		public FakeBlockAccess(Block block) {
			this.block = block;
		}

		@Nullable
		@Override
		public TileEntity getTileEntity(BlockPosition blockPosition) {
			return null;
		}

		@Override
		public IBlockData getType(BlockPosition blockPosition) {
			return this.block.getBlockData();
		}

		@Override
		public Fluid getFluid(BlockPosition blockPosition) {
			return null;
		}
	}
}
