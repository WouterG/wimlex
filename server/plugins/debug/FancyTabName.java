import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import net.menoni.wimlex.addons.Beans;
import net.menoni.wimlex.addons.beans.singleton.PlayerColorHandler;
import net.menoni.wimlex.addons.model.WPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.Team;

/**
 * Created by wouter on 8/28/16.
 */
public class FancyTabName implements Debugger, Debugger.Stoppable, Runnable {

	private ChatColor[] colors = new ChatColor[] {
			ChatColor.LIGHT_PURPLE,
			ChatColor.DARK_PURPLE,
			ChatColor.DARK_RED,
			ChatColor.RED,
			ChatColor.GOLD,
			ChatColor.YELLOW,
			ChatColor.DARK_GREEN,
			ChatColor.GREEN,
			ChatColor.AQUA,
			ChatColor.DARK_AQUA,
			ChatColor.BLUE
	};

	private int startIndex = 0;

	private String birthdayBoyUsername = "Notenkraker_08";
	private BukkitTask task;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		this.task = Bukkit.getScheduler().runTaskTimer(plugin, this, 2, 2);
	}

	@Override
	public void run() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!p.getName().equalsIgnoreCase(birthdayBoyUsername)) {
				continue;
			}
			int cur = startIndex;
			String name = "";
			for (char c : p.getName().toCharArray()) {
				while (cur < 0) {
					cur = this.colors.length - 1;
				}
				name += colors[cur--].toString() + c;
			}
			startIndex--;
			while (startIndex < 0) {
				startIndex = this.colors.length - 1;
			}
			updateListName(p, name);
		}
	}

	public void updateListName(Player p, String name) {
		Team team = Bukkit.getScoreboardManager().getMainScoreboard().getEntryTeam(p.getName());
		String prefix = "";
		if (team != null) {
			prefix = ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + team.getDisplayName() + ChatColor.DARK_GRAY + "] ";
		}
		p.setPlayerListName(prefix + name);
	}

	@Override
	public void stop() {
		this.task.cancel();
		for (Player p : Bukkit.getOnlinePlayers()) {
			WPlayer player = Beans.getPlugin().getPlayer(p.getUniqueId());
			Beans.getBean(PlayerColorHandler.class).updatePlayerTabName(p, player);
		}
	}
}