import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullTest implements Debugger {
	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		ItemStack is = new ItemStack(Material.PLAYER_HEAD, 1);
		SkullMeta meta = ((SkullMeta)is.getItemMeta());
		meta.setOwningPlayer(Bukkit.getOfflinePlayer("Etho"));
		is.setItemMeta(meta);
		((Player)cs).getInventory().addItem(is);
	}
}
