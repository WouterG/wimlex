import net.menoni.rd.RuntimeDebugger;
import net.menoni.rd.model.Debugger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitTask;

public class MapDiscover implements Debugger, Runnable, Debugger.Stoppable {

	private Player p;
	private BukkitTask task;

	private int startX = 800;
	private int startZ = 0;
	private int endX = 10000;
	private int endZ = -10000;

	private int step = 64;

	private int count = 0;
	private int rowMoved = 0;

	private int lowX = Math.min(startX, endX);
	private int highX = Math.max(startX, endX);
	private int lowZ = Math.min(startZ, endZ);
	private int highZ = Math.max(startZ, endZ);

	private int x;
	private int z;

	@Override
	public void debug(RuntimeDebugger plugin, CommandSender cs) {
		if (!(cs instanceof Player)) {
			return;
		}
		p = (Player) cs;
		task = Bukkit.getScheduler().runTaskTimer(plugin, this, 10L, 10L);
		this.x = lowX;
		this.z = lowZ;
	}

	@Override
	public void run() {
		count++;
		if (count % 5 == 0 || (rowMoved > 0 && rowMoved <= 5)) {
			if (rowMoved > 0) {
				rowMoved--;
			}
			return;
		}
		p.teleport(
				new Location(
						p.getWorld(),
						x,
						p.getLocation().getY(),
						z
				),
				PlayerTeleportEvent.TeleportCause.PLUGIN
		);
		if (rowMoved > 5) {
			rowMoved--;
			return;
		}
		z += step;
		if (z >= highZ) {
			p.sendMessage(ChatColor.YELLOW + "a row done");
			z = lowZ;
			x += (step * 2);
			rowMoved = 6;
		}
		if (x >= highX) {
			p.sendMessage(ChatColor.GREEN + "done");
			this.task.cancel();
		}
	}

	@Override
	public void stop() {
		this.task.cancel();
	}
}