#!/bin/bash

source ~/config/setup.sh

RUNNING=false

if screen -list | grep -i "\.$SPIGOT_SCREENNAME"; then
	RUNNING=true
fi

if [ "$SPIGOT_UPDATE" = "true" ]; then
	if [ "$RUNNING" = "true" ]; then
		~/cron/send-notice.sh pre-download
	fi
	~/cron/spigot-update.sh download
	if [ "$RUNNING" = "true" ]; then
		~/cron/send-notice.sh post-download
	fi
fi

if [ "$RUNNING" = "true" ]; then
	sleep 5s
	~/cron/send-notice.sh 5sec
	sleep 4s
	~/cron/send-notice.sh 1sec
	sleep 1s
	~/server/send-cmd.sh "save-all"
	sleep 10s
	~/server/send-cmd.sh "stop"
	sleep 10s
	screen -X -S $SPIGOT_SCREENNAME quit
	sleep 3s
fi

if [ "$SPIGOT_BACKUP" = "true" ]; then
	case "$SPIGOT_BACKUP_METHOD" in
		LOCAL)
			~/cron/local-backup.sh
			;;
		GIT)
			~/cron/git-backup.sh
			;;
		NFS)
			~/cron/nfs-backup.sh
			;;
	esac
	if [ "$SPIGOT_BACKUP_METHOD" != "GIT" ] && [ "$SPIGOT_GIT_CONFIG_SYNC" = "true" ]; then
		~/cron/git-config-sync.sh
	fi
fi

if [ "$SPIGOT_UPDATE" = "true" ]; then
	~/cron/spigot-update.sh install
fi

~/server/screen-start.sh
