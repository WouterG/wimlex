#!/bin/bash

SPIGOT_RESTARTNOTICE_10MIN="Restarting in 10 minutes to backup & update"
SPIGOT_RESTARTNOTICE_5MIN="Restarting in 5 minutes to backup & update"
SPIGOT_RESTARTNOTICE_1MIN="Restarting in about 1 minute to backup & update"
SPIGOT_RESTARTNOTICE_PRE_DOWNLOAD="Installing server update - server may lag a bit"
SPIGOT_RESTARTNOTICE_POST_DOWNLOAD="Server update installed and ready - restarting now."
SPIGOT_RESTARTNOTICE_5SEC="Restarting in 5 seconds to backup & update"
SPIGOT_RESTARTNOTICE_1SEC="Restarting now..."

