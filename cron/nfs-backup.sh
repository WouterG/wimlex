#!/bin/bash
cd ~
mkdir tmp-worlds
cp -r server/worlds ./tmp-worlds

zip -r worlds.zip tmp-worlds &> /dev/null && scp ~/worlds.zip wouter@menoni.net:~/wimlex &> /dev/null && cd ~ && rm -r ~/tmp-worlds && rm ~/worlds.zip &
