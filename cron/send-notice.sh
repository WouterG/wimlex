#!/bin/bash

source ~/cron/restart-notice-texts.sh

if [ "$#" -lt 1 ]; then
	echo "Usage: $0 <10m|5m|1m|pre-download|post-download|5sec|1sec>"
	exit 1
fi

case "$1" in

10m)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_10MIN"
	exit 1
	;;
5m)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_5MIN"
	exit 1
	;;
1m)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_1MIN"
	exit 1
	;;
pre-download)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_PRE_DOWNLOAD"
	exit 1
	;;
post-download)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_POST_DOWNLOAD"
	exit 1;
	;;
5sec)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_5SEC"
	exit 1
	;;
1sec)
	~/server/send-say.sh "$SPIGOT_RESTARTNOTICE_1SEC"
	exit 1
	;;
*)
	echo "Invalid input"
	exit 1
	;;
esac
