#!/bin/bash

if [ "$#" -lt 1 ]; then
	echo "Usage: $0 <search>"
	echo -e "or $0 \x22<search words>\x22"
	exit 1
fi

cd ~/log-scan
rm -rf ~/log-scan/work
mkdir ~/log-scan/work
cp ~/server/logs/* ~/log-scan/work
cd ~/log-scan/work
for f in *.gz ; do gunzip -c "$f" > ~/log-scan/work/"${f%.*}" ; done
rm -rf *.gz
grep -rnw "$@" *.log
