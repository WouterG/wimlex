#!/bin/bash

cd ~
source ~/config/setup.sh

if [ "$SPIGOT_UPDATE" = "true" ]; then
	~/cron/spigot-update.sh download
	~/cron/spigot-update.sh install
fi

~/server/screen-start.sh
