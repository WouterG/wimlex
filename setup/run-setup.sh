#!/bin/bash

~/server/stream-cmd-file.sh ~/setup/mc-cmds-gamerules
~/server/stream-cmd-file.sh ~/setup/mc-cmds-teams
~/server/stream-cmd-file.sh ~/setup/mc-cmds-whitelist
~/server/stream-cmd-file.sh ~/setup/mc-cmds-deathcount-scoreboard
~/server/stream-cmd-file.sh ~/setup/mc-cmds-permissions
